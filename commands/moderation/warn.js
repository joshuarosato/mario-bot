const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');
const getUserNick = require('../../utils/getUserNick');

class Warn extends Command {
    constructor() {
        super('warn', {
            aliases: ['warn'],
            category: 'moderation',
            channel: 'guild',
            // clientPermissions: [],
            // userPermissions: [],
            args: [
                {
                    id: 'member',
                    type: 'member'
                },
                {
                    id: 'reason',
                    type: 'string',
                    match: 'rest'
                }
            ],
            description: {
                content: 'Warn a member.',
                usage: '<user> <reason>',
                examples: ['@MEE6 bad bot']
            }
        });
    }

    exec(message, args) {
        if (!args.member) {
            return message.reply('Please specify a member to warn.')
        }

        if (!args.reason) {
            return message.reply('Please specify a reason for warning.')
        }

        let userNick = getUserNick(args.member)

        const msgEmbed = new MessageEmbed()
            .setTitle('WARNING!')
            .setImage('https://media1.tenor.com/images/1f9c9f384437d20c30c4791506e00bd7/tenor.gif?itemid=5633151')
            .setDescription(`**${userNick}** was warned for **${args.reason}**.`)
            .setColor('#FF4136')
            // .setFooter(`${message.author.username}#${message.author.discriminator}`)
            .setTimestamp()

        // async function before enabling
        // await args.member.send(`You have been warned in **${message.guild.name}** for the following reason: "**${args.reason}**"`)
        //     .catch(() => console.log('Could not send message to the concerned user.'));

        return message.channel.send(msgEmbed);
    }
}

module.exports = Warn;
