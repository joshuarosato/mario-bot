const { Command } = require('discord-akairo');

class Purge extends Command {
    constructor() {
        super('purge', {
            aliases: ['purge', 'prune', 'clean', 'clear'],
            category: 'moderation',
            channel: 'guild',
            clientPermissions: ['MANAGE_MESSAGES'],
            userPermissions: ['MANAGE_MESSAGES'],
            args: [
                {
                    id: 'amount',
                    type: 'integer',
                    default: 10
                }
            ],
            description: {
                content: 'Bulk delete messages.',
                usage: '[amount]',
                examples: ['50']
            }
        });
    }

    async exec(message, args) {
        if (args.amount >= 100) {
            return message.reply('Please enter a number between 1 and 99.')
        }

        message.channel.bulkDelete(args.amount + 1, false)
            .then(messages => message.channel.send(`Deleted ${messages.size} messages.`))
            .catch(error => message.channel.send(`An error occurred: ${error}`));
    }
}

module.exports = Purge;
