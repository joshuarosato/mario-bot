const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');
const getUserNick = require('../../utils/getUserNick');

class Kick extends Command {
    constructor() {
        super('kick', {
            aliases: ['kick', 'yeet'],
            category: 'moderation',
            channel: 'guild',
            clientPermissions: ['KICK_MEMBERS'],
            userPermissions: ['KICK_MEMBERS'],
            args: [
                {
                    id: 'member',
                    type: 'member'
                },
                {
                    id: 'reason',
                    type: 'string',
                    match: 'rest'
                }
            ],
            description: {
                content: 'Kick a member.',
                usage: '<user> <reason>',
                examples: ['@MEE6 bad bot']
            }
        });
    }

    async exec(message, args) {
        if (!args.member) {
            return message.reply('Please specify a member to kick.')
        }
        if (!args.reason) {
            return message.reply('Please specify a reason for kicking.')
        }
        if (args.member.id === this.client.user.id) {
            return message.channel.send('Why are you trying to kick me?');
        }
        if (args.member.id === message.author.id) {
            return message.channel.send('Why would you kick yourself?');
        }

        // await member.send(`You have been kicked from **${message.guild.name}** for the following reason: "**${args.reason}**"`)
        //     .catch(() => console.log('Could not send message to the concerned user.'));

        let userNick = getUserNick(args.member)

        const msgEmbed = new MessageEmbed()
            .setTitle('KICKED!')
            .setImage('https://media1.tenor.com/images/ca1bad80a757fa8b87dacd9c051f2670/tenor.gif?itemid=11029651')
            .setDescription(`**${userNick}** was kicked for **${args.reason}**.`)
            .setColor('#FF4136')
            // .setFooter(`${message.author.username}#${message.author.discriminator}`)
            .setTimestamp()

        // return args.member.kick({ reason: `Kicked by: ${message.author.username} for the following reason: ${args.reason}` })
        //     .then(() => message.channel.send(msgEmbed))
        //     .catch(err => console.error(err));

        return message.channel.send(msgEmbed)
    }
}

module.exports = Kick;
