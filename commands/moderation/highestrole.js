const { Command } = require('discord-akairo');

class HighestRole extends Command {
    constructor() {
        super('highestrole', {
            aliases: ['highestrole'],
            category: 'moderation',
            channel: 'guild',
            // clientPermissions: [],
            // userPermissions: [],
            args: [
                {
                    id: 'member',
                    type: 'member',
                    prompt: {
                        start: 'Who would you like to get the highest role of?',
                        retry: 'That\'s not a valid member! Please try again.'
                    }
                }
            ],
            description: {
                content: 'Get the highest role of a member.',
                usage: '<user>',
                examples: ['@MEE6']
            }
        });
    }

    exec(message, args) {
        let userNick;

        if (args.member.nickname) {
            userNick = args.member.nickname
        } else {
            userNick = args.member.user.username
        }

        if (args.member.roles.highest.name === '@everyone') {
            return message.reply(`${userNick} doesn't have any roles.`)
        }

        return message.reply(`The highest role of ${userNick} is ${args.member.roles.highest.name}`);
    }
}

module.exports = HighestRole;
