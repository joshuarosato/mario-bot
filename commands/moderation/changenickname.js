const { Command } = require('discord-akairo');

class ChangeNickname extends Command {
    constructor() {
        super('changenickname', {
            aliases: ['changenickname', 'changenick', 'changeusernick', 'changeusernickname'],
            category: 'moderation',
            channel: 'guild',
            clientPermissions: ['MANAGE_NICKNAMES'],
            userPermissions: ['MANAGE_NICKNAMES'],
            args: [
                {
                    id: 'member',
                    type: 'member'
                },
                {
                    id: 'nickname',
                    type: 'string'
                },
                {
                    id: 'reason',
                    type: 'string',
                    match: 'rest'
                }
            ],
            description: {
                content: 'Change the nickname of a member.',
                usage: '<user> <nickname> <reason>',
                examples: ['@MEE6 BadBot because it kinda sucks']
            }
        });
    }

    exec(message, args) {
        if (!args.member) {
            return message.reply('Please specify a member.')
        }

        if (!args.nickname) {
            return message.reply('Please specify a new nickname for this member.')
        }

        let reason = args.reason || 'No reason specified'

        args.member.setNickname(args.nickname, reason)
            .then(message.channel.send(`Nickname updated to ${args.nickname} with reason: ${reason}`))
    }
}

module.exports = ChangeNickname;
