const { Command } = require('discord-akairo');

class Echo extends Command {
    constructor() {
        super('echo', {
            aliases: ['echo'],
            category: 'util',
            args: [
                {
                    id: 'msgContent',
                    type: 'string',
                    prompt: {
                        start: 'What would you like me to echo?'
                    },
                    match: 'rest'
                }
            ],
            description: {
                content: 'Echo what the user says.',
                usage: '<message>',
                examples: ['Hello world!']
            },
        });
    }

    exec(message, args) {
        return message.channel.send(args.msgContent);
    }
}

module.exports = Echo;
