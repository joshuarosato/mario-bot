const { Command } = require('discord-akairo');
const getUserNick = require('../../utils/getUserNick');

class Test extends Command {
    constructor() {
        super('test', {
            aliases: ['test'],
            category: 'util',
            channel: 'guild',
            args: [
                {
                    id: 'member',
                    type: 'member'
                }
            ]
        });
    }

    exec(message, args) {
        console.log(this.client.commandHandler)
    }
}

module.exports = Test;
