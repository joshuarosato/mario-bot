const { Command } = require('discord-akairo');

class Google extends Command {
    constructor() {
        super('google', {
            aliases: ['google', 'lmgtfy'],
            category: 'util',
            args: [
                {
                    id: 'msgContent',
                    type: 'string',
                    default: '',
                    match: 'content'
                }
            ]
        });
    }

    exec(message, args) {
        let query = args.msgContent.replace(' ', '+')
        return message.channel.send(`https://lmgtfy.com/?q=${query}`);
    }
}

module.exports = Google;
