const { Command } = require('discord-akairo');

class Nickname extends Command {
    constructor() {
        super('nickname', {
            aliases: ['nickname'],
            category: 'util',
            channel: 'guild',
            args: [
                {
                    id: 'member',
                    type: 'member'
                }
            ]
        });
    }

    exec(message, args) {
        if (args.member) {
            let userFullName = `${args.member.user.username}#${args.member.user.discriminator}`
            let userNickName = `${args.member.nickname}`

            if (!args.member.nickname) {
                return message.channel.send(`${userFullName} doesn't have a nickname in this server.`)
            } else {
                return message.channel.send(`The nickname of ${userFullName} is ${userNickName}`)
            }
        } else {
            return message.channel.send(`The nickname of ${message.member.user.username}#${message.member.user.discriminator} is ${message.member.nickname}`);
        }
    }
}

module.exports = Nickname;
