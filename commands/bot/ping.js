const { Command } = require('discord-akairo');

class Ping extends Command {
    constructor() {
        super('ping', {
            aliases: ['ping'],
            category: 'bot',
            description: {
                content: 'Check the connection to the bot.',
                usage: '',
                examples: ['']
            }
        });
    }

    exec(message) {
        return message.reply('Pong!');
    }
}

module.exports = Ping;
