const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');
const fetch = require('node-fetch')
const waifuAPI = 'https://waifu.pics/api/'
const endpoint = 'sfw/shinobu'

class Shinobu extends Command {
    constructor() {
        super('shinobu', {
            aliases: ['shinobu'],
            category: 'anime'
        });
    }

    async exec(message) {
        const { url } = await fetch(waifuAPI + endpoint).then(res => res.json())
        const msgEmbed = new MessageEmbed().setDescription('').setImage(url).setColor('#7289DA')
        message.channel.send(msgEmbed)
    }
}

module.exports = Shinobu;
