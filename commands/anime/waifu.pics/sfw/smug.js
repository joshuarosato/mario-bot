const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');
const fetch = require('node-fetch')
const waifuAPI = 'https://waifu.pics/api/'
const endpoint = 'sfw/smug'

class Smug extends Command {
    constructor() {
        super('smug', {
            aliases: ['smug'],
            category: 'anime'
        });
    }

    async exec(message) {
        const { url } = await fetch(waifuAPI + endpoint).then(res => res.json())
        const msgEmbed = new MessageEmbed().setDescription('').setImage(url).setColor('#7289DA')
        message.channel.send(msgEmbed)
    }
}

module.exports = Smug;
