const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');
const getUserNick = require('../../../../utils/getUserNick');
const fetch = require('node-fetch')
const waifuAPI = 'https://waifu.pics/api/'
const endpoint = 'sfw/bully'

class Bully extends Command {
    constructor() {
        super('bully', {
            aliases: ['bully'],
            category: 'anime',
            channel: 'guild',
            clientPermissions: ['EMBED_LINKS'],
            args: [
                {
                    id: 'member',
                    type: 'member',
                    prompt: {
                        start: 'Who would you like to bully?',
                        retry: 'That\'s not a valid member! Please try again.'
                    }
                }
            ],
            description: {
                content: 'Bully another member.',
                usage: '<member>',
                examples: ['@MEE6']
            }
        });
    }

    async exec(message, args) {
        const { url } = await fetch(waifuAPI + endpoint).then(res => res.json())
        let userNick = getUserNick(args.member)
        let authorNick = getUserNick(message.member)
        const msgEmbed = new MessageEmbed()
            .setImage(url)
            .setDescription(`${authorNick} bullied ${userNick}.`)
            .setColor('#7289DA')
            .setFooter(`${message.author.username}#${message.author.discriminator}`)
            .setTimestamp()
        return message.channel.send(msgEmbed)
    }
}

module.exports = Bully;


// https://anidiots.guide/understanding/roles#permission-code
// for (permission in clientPermissions) {
//     let has_perm = message.channel.permissionsFor(message.member).has(permission, false);

//     if (has_perm === false) {
//         message.channel.send(`Missing permission: ${permission}`)
//     }
// }
