const { Command } = require('discord-akairo');

class KillBot extends Command {
    constructor() {
        super('killbot', {
            aliases: ['killbot'],
            category: 'owner',
            ownerOnly: true,
            description: {
                content: 'Kill the bot.',
                usage: '',
                examples: ['']
            },
        });
    }

    async exec(message) {
        await message.channel.send(`Shutting down...`);
        process.exit();
    }
}

module.exports = KillBot;
