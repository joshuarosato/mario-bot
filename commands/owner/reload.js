const { Command } = require('discord-akairo');

class Reload extends Command {
    constructor() {
        super('reload', {
            aliases: ['reload'],
            category: 'owner',
            ownerOnly: true,
            args: [
                {
                    id: 'commandAlias',
                    type: 'commandAlias',
                    prompt: {
                        start: 'Which command would you like to reload?',
                        retry: 'That\'s not a valid command! Please try again.'
                    }
                }
            ],
            description: {
                content: 'Reload a command.',
                usage: '<command>',
                examples: ['ping']
            }
        });
    }

    exec(message, args) {
        this.handler.reload(args.commandAlias);
        return message.channel.send(`Reloaded command: ${args.commandAlias}!`);
    }
}

module.exports = Reload;
