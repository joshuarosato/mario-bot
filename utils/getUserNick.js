const { GuildMember, User } = require('discord.js');

function getUserNick(discordUser) {
    if (discordUser instanceof GuildMember) {
        return getGuildMemberObjectNick(discordUser)
    } else {
        if (discordUser instanceof User) {
            return getUserObjectNick(discordUser)
        }
    }
}

function getGuildMemberObjectNick(guildMemberObject) {
    if (guildMemberObject.nickname) {
        return guildMemberObject.nickname
    } else {
        return guildMemberObject.user.username
    }
}

function getUserObjectNick(userObject) {
    return userObject.username
}

module.exports = getUserNick;
