const { AkairoClient, CommandHandler, InhibitorHandler, ListenerHandler } = require('discord-akairo');
const config = require('./config.json');

class MyClient extends AkairoClient {
    constructor() {
        super({
            ownerID: config.ownerID,
            presence: {
                status: 'online',
                activity: {
                    type: 'PLAYING',
                    name: '>>help'
                }
            }
        }, {
            disableMentions: 'everyone'
        });

        this.commandHandler = new CommandHandler(this, {
            directory: './commands/',
            prefix: config.prefix,
            defaultCooldown: 5000,
            commandUtil: true,
            commandUtilLifetime: 60000,
            allowMention: true,
            handleEdits: true,
            ignorePermissions: config.ownerID,
            ignoreCooldown: config.ownerID
        });

        this.inhibitorHandler = new InhibitorHandler(this, {
            directory: './events/inhibitors/',
            emitters: { process },
        });

        this.listenerHandler = new ListenerHandler(this, {
            directory: './events/listeners/'
        });

        this.listenerHandler.setEmitters({
            commandHandler: this.commandHandler,
            inhibitorHandler: this.inhibitorHandler,
            listenerHandler: this.listenerHandler,
            process: process
        });

        this.commandHandler.useInhibitorHandler(this.inhibitorHandler);
        this.commandHandler.useListenerHandler(this.listenerHandler);

        this.listenerHandler.loadAll();
        this.inhibitorHandler.loadAll();
        this.commandHandler.loadAll();
    }
}

const client = new MyClient();
client.login(config.botToken);
